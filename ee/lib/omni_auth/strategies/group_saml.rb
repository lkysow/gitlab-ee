module OmniAuth
  module Strategies
    class GroupSaml < SAML
      option :name, 'group_saml'
    end
  end
end
